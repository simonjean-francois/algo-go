var diagram = null;
var codeTextArea = document.getElementById('code');
var opCounter = 2; // Commencer par op2
var condCounter = 2; // Commencer par cond2

function addElement(type) {
    var startPosition = codeTextArea.selectionStart;
    var endPosition = codeTextArea.selectionEnd;
    var code = codeTextArea.value;
    var elementText = '';

    switch (type) {
        case 'start':
            elementText = 'st=>start: Début';
            break;
        case 'operation':
            var operationName = prompt("Entrez le texte pour l'action :");
            if (operationName !== null && operationName !== "") {
                elementText = 'op' + (opCounter++) + '=>operation: ' + operationName;
            }
            break;
        case 'condition':
            var conditionText = prompt("Entrez le texte pour la condition :");
            if (conditionText !== null && conditionText !== "") {
                elementText = 'cond' + (condCounter++) + '=>condition: ' + conditionText;
            }
            break;
        case 'end':
            elementText = 'e=>end: Fin';
            break;
        default:
            break;
    }

    if (elementText !== '') {
        var newCode = code.slice(0, startPosition) + '\n' + elementText + '\n' + code.slice(endPosition);
        codeTextArea.value = newCode;
    }
}

function applyChanges() {
    var code = codeTextArea.value;
    var flowchartContainer = document.getElementById('flowchartContainer');

    // Supprimer le diagramme existant s'il y en a un
    while (flowchartContainer.firstChild) {
        flowchartContainer.removeChild(flowchartContainer.firstChild);
    }

    // Créer un nouveau div pour le diagramme
    var newFlowchartDiv = document.createElement('div');
    newFlowchartDiv.id = 'flowchart';
    flowchartContainer.appendChild(newFlowchartDiv);

    // Dessiner le nouveau diagramme
    diagram = flowchart.parse(code);
    diagram.drawSVG('flowchart', {
        'line-width': 2,
        'line-length': 50,
        'text-margin': 10,
        'font-size': 14,
        'font-color': 'black',
        'line-color': 'black',
        'element-color': 'black',
        'fill': 'white',
        'yes-text': 'Oui',
        'no-text': 'Non',
        'arrow-end': 'block',
        'scale': 1,
        // style symbol types
        'symbols': {
            'start': {
                'font-color': 'red',
                'element-color': 'green',
                'fill': 'yellow'
            },
            'end': {
                'class': 'end-element'
            }
        },
        // even flowstate support ;-)
        'flowstate': {
            // 'past': { 'fill': '#CCCCCC', 'font-size': 12},
            // 'current': {'fill': 'yellow', 'font-color': 'red', 'font-weight': 'bold'},
            // 'future': { 'fill': '#FFFF99'},
            'request': { 'fill': 'blue'}//,
            // 'invalid': { 'fill': '#444444'},
            // 'approved': { 'fill': '#58C4A3'},
            // 'rejected': { 'fill': '#C45879'}
        }
    });
}

function exportDiagramWithText() {
    exportDiagram(true);
}

function exportDiagramWithoutText() {
    exportDiagram(false);
}

function exportDiagram(withText) {
    var svg = document.getElementById('flowchart').innerHTML;

    // Supprimer le texte du SVG si nécessaire
    if (!withText) {
        var parser = new DOMParser();
        var doc = parser.parseFromString(svg, "image/svg+xml");
        var texts = doc.querySelectorAll('text');
        for (var i = 0; i < texts.length; i++) {
            texts[i].textContent = ''; // Effacer le texte
        }
        svg = doc.documentElement.outerHTML;
    }

    // Créer un canevas pour le rendu SVG
    var canvas = document.createElement('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    // Utiliser canvg pour rendre le SVG sur le canevas
    canvg(canvas, svg);

    // Obtenir l'URL de l'image à partir du canevas
    var imgURL = canvas.toDataURL('image/png');

    // Créer un lien de téléchargement pour l'image
    var link = document.createElement('a');
    link.download = 'diagramme.png';
    link.href = imgURL;

    // Simuler un clic sur le lien pour télécharger l'image
    link.click();
}

function saveAlgorithm() {
    var algorithmText = codeTextArea.value;
    var blob = new Blob([algorithmText], { type: 'text/plain;charset=utf-8' });
    saveAs(blob, 'algorithm.txt');
}
